package com.toocms.sample.data;

/**
 * Author：Zero
 * Date：2020/11/5 15:27
 */
public class User {

    public String m_id;
    public String account;
    public String nickname;
    public String avatar;
    public String balance;
    public String member_expiration_date;
    public String status;
    public String account_format;
    public String avatar_path;
    public String is_pay_pass;
    public String is_super_member;
    public String default_address;

    @Override
    public String toString() {
        return "User{" +
                "m_id='" + m_id + '\'' +
                ", account='" + account + '\'' +
                ", nickname='" + nickname + '\'' +
                ", avatar='" + avatar + '\'' +
                ", balance='" + balance + '\'' +
                ", member_expiration_date='" + member_expiration_date + '\'' +
                ", status='" + status + '\'' +
                ", account_format='" + account_format + '\'' +
                ", avatar_path='" + avatar_path + '\'' +
                ", is_pay_pass='" + is_pay_pass + '\'' +
                ", is_super_member='" + is_super_member + '\'' +
                ", default_address='" + default_address + '\'' +
                '}';
    }
}
