package com.toocms.sample.config;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.toocms.sample.ui.expand.push.PushFgt;
import com.toocms.tab.configs.IAppConfig;
import com.toocms.tab.map.TabMapApi;
import com.toocms.tab.network.encrypt.ApiEncryptOption;
import com.toocms.tab.push.TabPush;
import com.toocms.tab.share.TabShare;
import com.umeng.message.UmengNotificationClickHandler;
import com.umeng.message.entity.UMessage;
import com.umeng.socialize.PlatformConfig;

import okhttp3.OkHttpClient;
import rxhttp.wrapper.param.Method;
import rxhttp.wrapper.param.Param;

/**
 * App配置
 * Author：Zero
 * Date：2020/11/06
 */
public class AppConfig implements IAppConfig {

    @Override
    public String getBaseUrl() {
        return "http://xlg-api.uuudoo.com/";
    }

    @Override
    public String getUpdateUrl() {
        return "http://hlhd-api.uuudoo.com/system/checkVersion";
    }

    @Override
    public String getUmengAppkey() {
        return "63bfc4fdd64e686139163c4d";
    }

    @Override
    public String getUmengPushSecret() {
        return "be3fe890e512358149268429f9e77274";
    }

    @Override
    public Param<?> setOnParamAssembly(Param<?> param) {
        Method method = param.getMethod();
//        if (method.isGet()) {
//            param.add("method", "get");
//        } else if (method.isPost()) { //Post请求
//            param.add("method", "post");
//        }
//        return param.add("versionName", "1.0.0")//添加公共参数
//                .add("time", System.currentTimeMillis())
//                .addHeader("deviceType", "android"); //添加公共请求头
        return param;
    }

    @Override
    public ApiEncryptOption getApiEncryptOption() {
        ApiEncryptOption option = new ApiEncryptOption();
        option.setPublicKey("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCYHGvdORdwsK5i+s9rKaMPL1O5eDK2XwNHRUWaxmGB/cxLxeinJrrqdAN+mME7XtGN9bklnOR3MUBQLVnWIn/IU0pnIJY9DpPTVc7x+1zFb8UUq1N0BBo/NpUG5olxuQULuAAHZOg28pnP/Pcb5XVEvpNKL0HaWjN8pu/Dzf8gZwIDAQAB");
        option.setAddHeader(false);
        option.setParamName("data");
        option.setOutputRealParam(true);
//        return option;
        return null;
    }

    @Override
    public void customOkHttp(OkHttpClient.Builder builder) {

    }

    @Override
    public boolean isCustomUpdate() {
        return false;
    }

    @Override
    public void initJarForWeApplication(Application application) {
        // 第三方账号所对应的唯一标识
        TabShare.registerQQ(application, "1105240612", "2iDzT9GvL7L9Q9bg");
        TabShare.registerWX(application, "wx0f7b622f264f30f5", "c5da5d5cc432d38358a850ad20f4f9fc");
        // 自行添加的分享
        PlatformConfig.setSinaWeibo("1008244763", "bd40713f78c08084bcdc3b49c358fb1b", "http://sns.whalecloud.com");
        PlatformConfig.setSinaFileProvider(application.getPackageName() + ".fileprovider");
        // 注册推送服务
        TabPush.getInstance().register(new UmengNotificationClickHandler() {

            @Override
            public void dealWithCustomAction(Context context, UMessage uMessage) {
                super.dealWithCustomAction(context, uMessage);
                Bundle bundle = new Bundle();
                bundle.putString("111", uMessage.extra.get("111"));
                bundle.putString("222", uMessage.extra.get("222"));
                TabPush.getInstance().startFragment(context, PushFgt.class, bundle);
            }
        });
        TabPush.getInstance().registerXiaoMiPush("2882303761519902601", "5561990252601");
        // 同意地图隐私政策
        TabMapApi.agreePrivacy(application);
    }
}
