package com.toocms.sample.ui.tool.image;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.LogUtils;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.interfaces.OnResultCallbackListener;
import com.toocms.sample.ui.widget.adder.AdderViewModel;
import com.toocms.tab.base.BaseViewModel;
import com.toocms.tab.binding.command.BindingCommand;
import com.toocms.tab.configs.FileManager;
import com.toocms.tab.network.ApiTool;

import java.util.ArrayList;

/**
 * Author：Zero
 * Date：2020/11/12 14:57
 */
public class ImageViewModel extends BaseViewModel {

    public String urlCenter = "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=383304109,937640631&fm=11&gp=0.jpg";
    public String urlLeftTop = "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=818918895,1070782834&fm=11&gp=0.jpg";
    public String urlRightTop = "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=4277751004,3595306162&fm=26&gp=0.jpg";
    public String urlLeftBottom = "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=4204193996,2127491495&fm=26&gp=0.jpg";
    public String urlRightBottom = "https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2207936752,3352455107&fm=26&gp=0.jpg";
    public ObservableField<String> url1 = new ObservableField<>();
    public ObservableField<String> url2 = new ObservableField<>();
    public ObservableField<String> url3 = new ObservableField<>();

    public ImageViewModel(@NonNull Application application) {
        super(application);
    }

    public BindingCommand clearMemory = new BindingCommand(() -> {
//        FileManager.clearMemCache();
//        showToast("运存缓存清理完毕");
        startSelectSignImageAty(new OnResultCallbackListener<LocalMedia>() {
            @Override
            public void onResult(ArrayList<LocalMedia> result) {
                ApiTool.post("http://yananbao-api.uuudoo.com/System/upload")
                        .addLocalMedia("image", result)
                        .add("folder", 2)
                        .asTooCMSResponse(AdderViewModel.UploadBean.class)
                        .withViewModel(ImageViewModel.this)
                        .request(uploadBean -> {
                            url1.set(uploadBean.list.get(0).abs_url);
                        });
            }

            @Override
            public void onCancel() {

            }
        });
    });

    public BindingCommand clearCache = new BindingCommand(() -> {
//        FileManager.clearCacheFiles();
//        showToast("内存缓存清理完毕");
        startSelectMultipleImageAty(null, 9, new OnResultCallbackListener<LocalMedia>() {
            @Override
            public void onResult(ArrayList<LocalMedia> result) {
                ApiTool.post("http://yananbao-api.uuudoo.com/System/upload")
                        .addLocalMedia("image", result)
                        .add("folder", 2)
                        .asTooCMSResponse(AdderViewModel.UploadBean.class)
                        .withViewModel(ImageViewModel.this)
                        .request(uploadBean -> {
                            url2.set(uploadBean.list.get(0).abs_url);
                            url3.set(uploadBean.list.get(1).abs_url);
                        });
            }

            @Override
            public void onCancel() {

            }
        });
    });
}
