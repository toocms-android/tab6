package com.toocms.sample.ui.tool;

import com.toocms.sample.R;
import com.toocms.sample.ui.base.BaseTabItemFgt;
import com.toocms.sample.ui.base.TabItem;
import com.toocms.sample.ui.tool.http.HttpFgt;
import com.toocms.sample.ui.tool.image.ImageFgt;
import com.toocms.sample.ui.tool.md5.MD5Fgt;
import com.toocms.sample.ui.tool.user.UserFgt;
import com.toocms.tab.TooCMSApplication;
import com.toocms.tab.expand.agreement.AgreementDialog;
import com.toocms.tab.widget.update.UpdateManager;

/**
 * Author：Zero
 * Date：2020/10/12 14:52
 */
public class ToolFgt extends BaseTabItemFgt {

    @Override
    protected void onFragmentCreated() {
        super.onFragmentCreated();
        topBar.setTitle("工具");
    }

    @Override
    protected TabItem[] getTabItems() {
        return new TabItem[]{
                new TabItem(R.drawable.ic_tool_tab_item_network, "网络请求", HttpFgt.class),
                new TabItem(R.drawable.ic_tool_tab_item_imageload, "图片加载", ImageFgt.class),
                new TabItem(R.drawable.ic_tool_tab_item_update, "版本更新", () -> UpdateManager.checkUpdate(true)),
                new TabItem(R.drawable.ic_tool_tab_item_md5, "MD5", MD5Fgt.class),
                new TabItem(R.drawable.ic_tool_tab_item_user, "用户信息", UserFgt.class),
                new TabItem(R.drawable.ic_tool_tab_item_user, "隐私协议", () ->
                        new AgreementDialog()
                                .setAgreementClickListener(view -> startFragment(HttpFgt.class))    // 用户协议点击事件
                                .setPolicyClickListener(view -> showToast("隐私政策"))  // 隐私政策点击事件
                                .setAgreeClickListener(view -> showToast("同意"))    // 同意按钮点击事件（无特殊逻辑可不调用）
                                .setRefuseClickListener(view -> showToast("拒绝"))    // 拒绝按钮点击事件（无特殊逻辑可不调用）
                                .show(getParentFragmentManager(), "Agreement"))      // 显示弹框
        };
    }
}
