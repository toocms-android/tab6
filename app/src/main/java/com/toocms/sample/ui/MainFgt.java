package com.toocms.sample.ui;

import com.toocms.sample.R;
import com.toocms.sample.ui.expand.ExpandFgt;
import com.toocms.sample.ui.expand.push.PushFgt;
import com.toocms.sample.ui.tool.ToolFgt;
import com.toocms.sample.ui.widget.WidgetFgt;
import com.toocms.tab.expand.tabsegment.BaseBottomTabSegmentFragment;
import com.toocms.tab.expand.tabsegment.TabSegmentItem;
import com.toocms.tab.push.TabPush;
import com.umeng.analytics.MobclickAgent;

/**
 * 首页
 * <p>
 * Author：Zero
 * Date：2020/9/29 18:23
 */
public class MainFgt extends BaseBottomTabSegmentFragment {

    @Override
    protected TabSegmentItem[] getTabSegmentItems() {
        return new TabSegmentItem[]{
                new TabSegmentItem(R.drawable.ic_tab_tool_normal, R.drawable.ic_tab_tool_selected, "工具", ToolFgt.class, false),
                new TabSegmentItem(R.drawable.ic_tab_widget_normal, R.drawable.ic_tab_widget_selected, null, WidgetFgt.class, false),
                new TabSegmentItem(R.drawable.ic_tab_expand_normal, R.drawable.ic_tab_expand_selected, "拓展", ExpandFgt.class, false)
        };
    }

    @Override
    protected boolean isSwipeable() {
        return false;
    }

    @Override
    protected boolean isEnableUmStatistics() {
        return false;
    }

    @Override
    protected void onBackPressed() {
        MobclickAgent.onKillProcess(getContext());
        super.onBackPressed();
    }
}
