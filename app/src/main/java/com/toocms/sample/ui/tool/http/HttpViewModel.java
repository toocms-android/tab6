package com.toocms.sample.ui.tool.http;

import android.app.Application;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.interfaces.OnResultCallbackListener;
import com.toocms.sample.parser.TestParser;
import com.toocms.tab.base.BaseModel;
import com.toocms.tab.base.BaseViewModel;
import com.toocms.tab.binding.command.BindingCommand;
import com.toocms.tab.bus.event.SingleLiveEvent;
import com.toocms.tab.configs.FileManager;
import com.toocms.tab.network.ApiTool;
import com.toocms.tab.network.HttpParams;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;

/**
 * Author：Zero
 * Date：2020/10/11 18:29
 */
public class HttpViewModel extends BaseViewModel<BaseModel> {

    private UIChangeObservable uc = new UIChangeObservable();
    public SingleLiveEvent<Integer> progress = new SingleLiveEvent<>();

    public HttpViewModel(@NonNull Application application) {
        super(application);
//        start();
    }

    public UIChangeObservable getUc() {
        return uc;
    }

    /**
     * Get请求方式（回调在主线程执行）按钮点击命令
     * 注：需在{@link #onDestroy()}中进行手动关闭请求
     * <p>
     *
     * @<code>if (! disposable.isDisposed ()) {  //判断请求有没有结束
     * disposable.dispose();  //没有结束，则关闭请求
     * }</code>
     */
    public BindingCommand getObjectOnMain = new BindingCommand(() ->
            ApiTool.get("Index/index")
                    .setAssemblyEnabled(false)
                    .asTooCMSResponse(Index.class)
                    .observeOn(AndroidSchedulers.mainThread())
                    .request(index -> uc.setText.setValue(index.toString())));

    public BindingCommand getListOnMain = new BindingCommand(() ->
            ApiTool.get("http://api.qunyan.uuudoo.com/Member/getMemberList")
                    .asTooCMSResponseList(User.class)
                    .observeOn(AndroidSchedulers.mainThread())
                    .request(users -> uc.setText.setValue(users.toString())));

    public BindingCommand customParser = new BindingCommand(() ->
            ApiTool.get("Index/index")
                    .setAssemblyEnabled(false)
                    .asCustomResponse(new TestParser<Index>() {
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .request(index -> uc.setText.setValue(index.toString())));

    //  Post请求方式（回调在主线程执行）按钮点击命令
    public BindingCommand postOnMain = new BindingCommand(() -> {
        ApiTool.post("http://hotpotshop-api.uuudoo.com/Center/setDefault")
                .add("m_id", 7)
                .add("adr_id", 5)
                .asTooCMSResponse(String.class)
                .observeOn(AndroidSchedulers.mainThread())
                .request(s -> uc.setText.setValue(s), throwable -> {
                });
    });

    //  Get请求方式（回调不在主线程执行）按钮点击命令
    public BindingCommand getNonMain = new BindingCommand(() ->
            ApiTool.get("http://api.qunyan.uuudoo.com/Member/getMemberList")
                    .asTooCMSResponseList(User.class)
                    .request(users -> ToastUtils.showShort(users.toString())));

    //  Post请求方式（回调不在主线程执行）按钮点击命令
    public BindingCommand postNonMain = new BindingCommand(() -> {
        HttpParams params = new HttpParams();
        params.put("m_id", 7);
        params.put("adr_id", 5);
        ApiTool.post("http://hotpotshop-api.uuudoo.com/Center/setDefault")
                .params(params)
                .asTooCMSResponse(String.class)
                .request(s -> ToastUtils.showShort(s));
    });

    //  Get请求方式（页面销毁自动关闭&自动显示/隐藏加载条）按钮点击命令
    public BindingCommand autoDispose = new BindingCommand(() ->
            ApiTool.get("Index/index")
                    .asTooCMSResponse(Index.class)
                    .withViewModel(this)
                    .request(index -> uc.setText.setValue(index.toString())));

    //  上传
    public BindingCommand upload = new BindingCommand(() ->
            startSelectSignImageAty(new OnResultCallbackListener<LocalMedia>() {

                @Override
                public void onResult(ArrayList<LocalMedia> result) {
                    ApiTool.post("http://xlg-api.uuudoo.com/System/upload")
                            .add("folder", "1")
                            .addFile("head", result.get(0).getCutPath())
                            .asUpload(progress -> HttpViewModel.this.progress.postValue(progress.getProgress()))
                            .asTooCMSResponse(ImageUrl.class)
                            .request(imageUrl -> uc.setText.setValue(imageUrl.toString()));
                }

                @Override
                public void onCancel() {

                }
            }));

    //  下载
    public BindingCommand download = new BindingCommand(() ->
            ApiTool.get("http://daxue100-1.oss-cn-qingdao.aliyuncs.com/DE0AA7F7AFF5CF55CAC5489B171D1B8A.pptx")
                    .asDownload(FileManager.getDownloadPath() + File.separator + System.currentTimeMillis() + ".pptx",
                            progress -> this.progress.postValue(progress.getProgress()))
                    .request(fileName -> uc.setText.setValue(fileName))
    );

    public class UIChangeObservable {
        public SingleLiveEvent<String> setText = new SingleLiveEvent<>();
    }

    //  刷新订单
    public void refresh_order() {
        ApiTool.post("http://daijia-admin-api.uuudoo.com/Index/refresh_order")
                .add("m_id", 7)
                .asTooCMSResponse(String.class)
                .withViewModel(this)
                .showLoading(false)
                .request(pushOrderBean -> {
                });
    }

    public void start() {
        ThreadUtils.executeByCustomAtFixRate(ThreadUtils.getSinglePool(), task, 2, TimeUnit.SECONDS);
    }

    public ThreadUtils.SimpleTask<String> task = new ThreadUtils.SimpleTask<String>() {
        @Override
        public String doInBackground() {
            return null;
        }

        @Override
        public void onSuccess(String result) {
            refresh_order();
        }
    };
}
