package com.toocms.sample.ui.widget.adder;

import android.app.Application;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.StringUtils;
import com.luck.picture.lib.entity.LocalMedia;
import com.toocms.tab.base.BaseModel;
import com.toocms.tab.base.BaseViewModel;
import com.toocms.tab.binding.command.BindingCommand;
import com.toocms.tab.network.ApiTool;
import com.toocms.tab.network.PostFormApi;
import com.toocms.tab.widget.pictureadder2.Picture;

import java.util.List;

/**
 * Author：Zero
 * Date：2020/11/3 14:09
 */
public class AdderViewModel extends BaseViewModel<BaseModel> {

    public ObservableList<LocalMedia> localMedias = new ObservableArrayList<>();

    public ObservableList<Picture> pictures = new ObservableArrayList<>();

    public AdderViewModel(@NonNull Application application) {
        super(application);
        member_info();
    }

    // 用户选择图片的结果，这里做上传图片操作
    public BindingCommand<List<LocalMedia>> result2 = new BindingCommand<>(localMedias -> {
        this.localMedias.clear();
        this.localMedias.addAll(localMedias);
    });

    // 获取用户相册已有照片并添加到控件中显示
    public void member_info() {
        ApiTool.post("http://jiaoyou-api.uuudoo.com/Personal/member_info")
                .add("m_id", 3)
                .asTooCMSResponse(InfoBean.class)
                .withViewModel(this)
                .showLoading(false)
                .request(infoBean -> {
                    pictures.clear();
                    pictures.addAll(infoBean.photo);
                });
    }

    /**
     * 用户选择图片的结果，这里做上传图片操作
     */
    public BindingCommand<List<LocalMedia>> result = new BindingCommand<>(localMedias -> {
        ApiTool.post("http://yananbao-api.uuudoo.com/System/upload")
                .addLocalMedia("image", localMedias)
                .add("folder", 1)
                .asTooCMSResponse(UploadBean.class)
                .withViewModel(this)
                .request(uploadBean -> {
                    // 更新数据源
                    pictures.addAll(uploadBean.list);
                    // 串连图片ID，请求更新接口（项目中可根据具体情况做逻辑判断）
                    member_update(join(pictures));
                });
    });

    /**
     * 删除图片回调
     * 返回值为删除后剩余的图片集合
     */
    public BindingCommand<List<Picture>> remove = new BindingCommand<>(pictures -> {
        // 这里接口逻辑是删除之后传一遍现有的图片ID进行覆盖
        // 如需获取已删除的图片调用 CollectionUtils.subtract(this.pictures,pictures);
        member_update(join(pictures));
    });

    /**
     * 更新用户相册
     *
     * @param ids 图片IDS
     */
    public void member_update(String ids) {
        ApiTool.post("http://jiaoyou-api.uuudoo.com/Personal/member_update")
                .add("m_id", 3)
                .add("photo", ids)
                .asTooCMSResponse(String.class)
                .withViewModel(this)
                .request();
    }

    /**
     * 将图片ID用","串连
     *
     * @param list
     * @return
     */
    public static String join(List<Picture> list) {
        if (CollectionUtils.isEmpty(list)) {
            return "";
        }
        StringBuilder joinStr = new StringBuilder();

        CollectionUtils.forAllDo(list, (index, item) -> {
            joinStr.append(item.id);
            if (index != list.size() - 1) {
                joinStr.append(",");
            }
        });
        return joinStr.toString();
    }

    public class InfoBean {
        public List<Picture> photo;
    }

    public class UploadBean {
        public List<Picture> list;
    }
}
