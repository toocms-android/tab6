package com.toocms.sample.ui.expand.advert;

import com.blankj.utilcode.util.ToastUtils;
import com.toocms.tab.expand.advert.BaseAdvertActivity;
import com.toocms.tab.imageload.ImageLoader;

/**
 * @Description:
 * @Author: Zero
 * @CreateDate: 2022/1/8 17:16
 * @UpdateUser:
 * @UpdateDate: 2022/1/8 17:16
 * @UpdateRemark:
 * @Version: 1.0
 */
public class AdvertAty extends BaseAdvertActivity {

    public String target_rule, param;

    @Override
    protected void onActivityCreated() {
        getAdvert("http://jiaoyou-api.uuudoo.com/Index/advert_lists", AdvertBean.class, advertBean -> {
            ImageLoader.loadUrl2Image(advertBean.picture, imgvAdvert, 0);
            target_rule = advertBean.target_rule;
            param = advertBean.param;
        });
    }

    @Override
    protected void onJumpClickOrFinish() {
        ToastUtils.showShort("跳过点击/倒计时完成事件");
    }

    @Override
    protected void onAdvertClick() {
        ToastUtils.showShort("广告图点击");
    }
}
