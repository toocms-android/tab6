package com.toocms.sample.ui.widget.floatlayout;

import android.app.Application;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.blankj.utilcode.util.ColorUtils;
import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.ResourceUtils;
import com.toocms.sample.R;
import com.toocms.tab.base.BaseViewModel;
import com.toocms.tab.binding.viewadapter.floatlayout.ViewAdapter;

import java.util.Random;

/**
 * Author：Zero
 * Date：2021/2/19
 */
public class FloatLayoutViewModel extends BaseViewModel {

    private final String[] strings = new String[]{"Android", "iOS", "PHP", "Html", "TooCMS", "Zero"};
    private Random random = new Random();

    public ObservableList<String> list = new ObservableArrayList<>();   // 数据源

    public FloatLayoutViewModel(@NonNull Application application) {
        super(application);
        // 模拟数据源，项目中会通过网络请求获取数据
        for (int i = 0; i < 20; i++) {
            list.add(strings[random.nextInt(strings.length)]);
        }
    }

    // TextViewItem样式
    public ViewAdapter.BindingTextView<String> textStyle = (textView, s) -> {
        // 设置边距
        int horizontalPadding = ConvertUtils.dp2px(15);
        int verticalPadding = ConvertUtils.dp2px(8);
        textView.setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
        // 字体加粗
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        // 设置文字
        textView.setText(s);
    };

    // TextViewItem点击监听
    public ViewAdapter.OnFloatItemClickListener<String> textListener = (textView, s) -> showToast(s);

    // CheckBoxItem样式
    public ViewAdapter.BindingCheckBox<String> checkStyle = (checkBox, s) -> {
        // 设置背景（可设置不同状态的背景）
        checkBox.setBackgroundResource(R.drawable.bg_tag);
        // 设置文字颜色（可设置不同状态的颜色）
        checkBox.setTextColor(new ColorStateList(
                //  "-"为未选中状态，下面数组则对应未选中状态的文字颜色
                new int[][]{{-android.R.attr.state_checked}, {android.R.attr.state_checked}},
                new int[]{ColorUtils.getColor(R.color.clr_main), Color.WHITE}));
        // 字体加粗
        checkBox.setTypeface(Typeface.DEFAULT_BOLD);
        // 设置文字
        checkBox.setText(s);
    };

    // CheckBoxItem状态改变监听
    public ViewAdapter.onFloatItemCheckListener<String> checkListener = (buttonView, isChecked, s) -> {
        if (isChecked) showToast("选中了" + s);
    };
}
