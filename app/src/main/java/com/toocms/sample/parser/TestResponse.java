package com.toocms.sample.parser;

import java.io.Serializable;

/**
 * @Description:
 * @Author: Zero
 * @CreateDate: 2021/12/24 15:30
 * @UpdateUser:
 * @UpdateDate: 2021/12/24 15:30
 * @UpdateRemark:
 * @Version: 1.0
 */
public class TestResponse<D> implements Serializable {

    /**
     * 标识
     */
    private String flag;

    /**
     * 信息
     */
    private String message;

    /**
     * 数据
     */
    private D data;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }
}
