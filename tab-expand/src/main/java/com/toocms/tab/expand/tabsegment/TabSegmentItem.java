package com.toocms.tab.expand.tabsegment;

import androidx.annotation.DrawableRes;
import androidx.fragment.app.Fragment;

/**
 * BaseTabSegmentFragment配置实体类
 * <p>
 * Author：Zero
 * Date：2020/10/15 14:45
 */
public class TabSegmentItem {

    public TabSegmentItem(int normalResId, int selectedResId, String text, Class<? extends Fragment> cls, boolean dynamicChangeIconColor) {
        this.normalResId = normalResId;
        this.selectedResId = selectedResId;
        this.text = text;
        this.cls = cls;
        this.dynamicChangeIconColor = dynamicChangeIconColor;
    }

    private @DrawableRes
    int normalResId;

    private @DrawableRes
    int selectedResId;

    private String text;

    private boolean dynamicChangeIconColor;

    private Class<? extends Fragment> cls;

    public int getNormalResId() {
        return normalResId;
    }

    public void setNormalResId(int normalResId) {
        this.normalResId = normalResId;
    }

    public int getSelectedResId() {
        return selectedResId;
    }

    public void setSelectedResId(int selectedResId) {
        this.selectedResId = selectedResId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDynamicChangeIconColor() {
        return dynamicChangeIconColor;
    }

    public void setDynamicChangeIconColor(boolean dynamicChangeIconColor) {
        this.dynamicChangeIconColor = dynamicChangeIconColor;
    }

    public Class<? extends Fragment> getCls() {
        return cls;
    }

    public void setCls(Class<? extends Fragment> cls) {
        this.cls = cls;
    }
}
