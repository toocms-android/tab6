package com.toocms.tab.expand.agreement;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.StringUtils;
import com.qmuiteam.qmui.util.QMUIResHelper;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton;
import com.toocms.tab.TooCMSApplication;
import com.toocms.tab.binding.command.BindingConsumer;
import com.toocms.tab.expand.R;

import org.jetbrains.annotations.NotNull;

/**
 * Description :用户协议&隐私政策弹框
 * Author : Zero
 * Date : 2021/10/26
 */
public class AgreementDialog extends DialogFragment {

    private TextView content;
    private QMUIRoundButton agree, refuse;

    private SpannableStringBuilder builder;
    private String appName, tips, agreement, policy;
    private int agreementStart, policyStart;

    private BindingConsumer<View> agreementClickListener;
    private BindingConsumer<View> policyClickListener;
    private BindingConsumer<View> agreeClickListener;
    private BindingConsumer<View> refuseClickListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.base_dialog_agreement, container, false);
        content = contentView.findViewById(R.id.content);
        agree = contentView.findViewById(R.id.agree);
        agree.setOnClickListener(view -> {
            TooCMSApplication.getInstance().setConsentAgreement(true);
            TooCMSApplication.getInstance().initializationSDK();
            if (null != agreeClickListener) agreeClickListener.call(view);
            dismiss();
        });
        refuse = contentView.findViewById(R.id.refuse);
        refuse.setOnClickListener(view -> {
            TooCMSApplication.getInstance().setConsentAgreement(false);
            if (null != refuseClickListener) refuseClickListener.call(view);
            AppUtils.exitApp();
            dismiss();
        });
        // 内容
        appName = StringUtils.getString(R.string.app_name);
        tips = StringUtils.getString(R.string.dialog_tips);
        agreement = StringUtils.getString(R.string.dialog_agreement);
        policy = StringUtils.getString(R.string.dialog_policy);
        tips = String.format(tips, appName, agreement, policy);
        agreementStart = tips.indexOf(agreement);
        policyStart = tips.indexOf(policy);
        builder = new SpannableStringBuilder(tips);
        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                if (null != agreementClickListener) agreementClickListener.call(widget);
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(QMUIResHelper.getAttrColor(getContext(), com.toocms.tab.R.attr.app_primary_color));
            }
        }, agreementStart, agreementStart + agreement.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                if (null != policyClickListener) policyClickListener.call(widget);
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(QMUIResHelper.getAttrColor(getContext(), com.toocms.tab.R.attr.app_primary_color));
            }
        }, policyStart, policyStart + policy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        content.setMovementMethod(new LinkMovementMethod());
        content.setText(builder);
        return contentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();

        if (null == dialog) {
            return;
        }
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setGravity(Gravity.CENTER);
        window.setLayout((int) (metrics.widthPixels * 0.8f), ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    /**
     * 设置用户协议点击监听
     *
     * @param clickListener
     * @return
     */
    public AgreementDialog setAgreementClickListener(@NonNull BindingConsumer<View> clickListener) {
        agreementClickListener = clickListener;
        return this;
    }

    /**
     * 设置隐私政策点击监听
     *
     * @param clickListener
     * @return
     */
    public AgreementDialog setPolicyClickListener(@NonNull BindingConsumer<View> clickListener) {
        policyClickListener = clickListener;
        return this;
    }

    /**
     * 设置同意按钮点击监听
     *
     * @param listener
     * @return
     */
    public AgreementDialog setAgreeClickListener(BindingConsumer<View> listener) {
        agreeClickListener = listener;
        return this;
    }

    /**
     * 设置拒绝按钮点击监听
     *
     * @param listener
     * @return
     */
    public AgreementDialog setRefuseClickListener(BindingConsumer<View> listener) {
        refuseClickListener = listener;
        return this;
    }

    @Override
    public int show(@NonNull FragmentTransaction transaction, @Nullable String tag) {
        if (TooCMSApplication.getInstance().isConsentAgreement()) return 0;
        return super.show(transaction, tag);
    }

    @Override
    public void show(@NonNull FragmentManager manager, @Nullable String tag) {
        if (TooCMSApplication.getInstance().isConsentAgreement()) return;
        super.show(manager, tag);
    }
}
