package com.toocms.tab.network.encrypt;

/**
 * @Description: API加密参数
 * @Author: Zero
 * @CreateDate: 2023/4/11 18:27
 */
public class ApiEncryptOption {

    /**
     * 加密公钥
     *
     * @return
     */
    private String publicKey;

    /**
     * 密文参数名称
     */
    private String paramName;

    /**
     * 转换类型，默认：RSA/None/PKCS1Padding
     */
    private String transformation;

    /**
     * 公钥长度，默认：publicKey.getBytes().length
     */
    private int keySize;

    /**
     * 是否加在header中，false为加在body中
     */
    private boolean isAddHeader;

    /**
     * 是否输出真实参数
     */
    private boolean isOutputRealParam;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getTransformation() {
        return transformation;
    }

    public void setTransformation(String transformation) {
        this.transformation = transformation;
    }

    public int getKeySize() {
        return keySize;
    }

    public void setKeySize(int keySize) {
        this.keySize = keySize;
    }

    public boolean isAddHeader() {
        return isAddHeader;
    }

    public void setAddHeader(boolean addHeader) {
        isAddHeader = addHeader;
    }

    public boolean isOutputRealParam() {
        return isOutputRealParam;
    }

    public void setOutputRealParam(boolean outputRealParam) {
        isOutputRealParam = outputRealParam;
    }
}
