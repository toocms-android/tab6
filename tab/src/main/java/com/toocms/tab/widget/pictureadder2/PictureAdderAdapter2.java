package com.toocms.tab.widget.pictureadder2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.toocms.tab.R;
import com.toocms.tab.binding.command.BindingCommand;
import com.toocms.tab.imageload.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Author：Zero
 * Date：2021/12/28 10:43
 *
 * @version v6.1.1
 */
public class PictureAdderAdapter2 extends RecyclerView.Adapter<PictureAdderAdapter2.ViewHolder> {

    public static final int TYPE_CAMERA = 1;
    public static final int TYPE_PICTURE = 2;
    private LayoutInflater mInflater;
    public BindingCommand<List<Picture>> onRemoveCommand;
    private List<Picture> pictures = new ArrayList<>();
    private int selectMax = 9;

    /**
     * 点击添加图片跳转
     */
    private OnAddPictureClickListener mOnAddPictureClickListener;
    private OnItemClickListener2 mOnItemClickListener2;

    public PictureAdderAdapter2(Context context, OnAddPictureClickListener mOnAddPictureClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mOnAddPictureClickListener = mOnAddPictureClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_filter_picture2, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //少于8张，显示继续添加的图标
        if (getItemViewType(position) == TYPE_CAMERA) {
            holder.mImg.setImageResource(R.drawable.ic_add_image);
            holder.mImg.setOnClickListener(v -> mOnAddPictureClickListener.onAddPictureClick());
            holder.mIvDel.setVisibility(View.INVISIBLE);
        } else {
            holder.mIvDel.setVisibility(View.VISIBLE);
            holder.mIvDel.setOnClickListener(view -> {
                int index = holder.getAdapterPosition();
                delete(index);
            });
            // 加载网络图片
            Picture picture = pictures.get(position);
            ImageLoader.loadUrl2Image(picture.abs_url, holder.mImg, com.luck.picture.lib.R.drawable.ps_image_placeholder);
            //itemView 的点击事件
            if (mOnItemClickListener2 != null) {
                holder.itemView.setOnClickListener(v -> {
                    int adapterPosition = holder.getAdapterPosition();
                    mOnItemClickListener2.onItemClick(adapterPosition, v);
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        if (pictures.size() < selectMax) {
            return pictures.size() + 1;
        } else {
            return pictures.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isShowAddItem(position)) {
            return TYPE_CAMERA;
        } else {
            return TYPE_PICTURE;
        }
    }

    private boolean isShowAddItem(int position) {
        int size = pictures.size() == 0 ? 0 : pictures.size();
        return position == size;
    }

    /**
     * 删除
     */
    public void delete(int position) {
        try {
            if (position != RecyclerView.NO_POSITION && pictures.size() > position) {
                pictures.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, pictures.size());
                if (onRemoveCommand != null) onRemoveCommand.execute(pictures);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOnRemovedCommand(BindingCommand<List<Picture>> onRemoveCommand) {
        this.onRemoveCommand = onRemoveCommand;
    }

    public void setSelectMax(int selectMax) {
        this.selectMax = selectMax;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
        notifyDataSetChanged();
    }

    public void remove(int position) {
        if (pictures != null) {
            pictures.remove(position);
        }
    }

    public void setOnItemClickListener(OnItemClickListener2 listener) {
        this.mOnItemClickListener2 = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mImg;
        ImageView mIvDel;

        public ViewHolder(View view) {
            super(view);
            mImg = view.findViewById(R.id.fiv);
            mIvDel = view.findViewById(R.id.iv_del);
        }
    }

    public interface OnAddPictureClickListener {
        void onAddPictureClick();
    }
}
