package com.toocms.tab.widget.pictureadder2;

import android.view.View;

/**
 * Author：Zero
 * Date：2020/1/21 14:53
 */
public interface OnItemClickListener2 {
    void onItemClick(int position, View v);
}
