package com.toocms.tab.widget.pictureadder2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.SelectModeConfig;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.interfaces.OnResultCallbackListener;
import com.toocms.tab.R;
import com.toocms.tab.binding.command.BindingCommand;
import com.toocms.tab.imageload.engine.CompressEngine;
import com.toocms.tab.imageload.engine.GlideEngine;
import com.toocms.tab.imageload.engine.SandboxEngine;
import com.toocms.tab.imageload.psui.PsUI;

import java.util.ArrayList;
import java.util.List;

/**
 * 图片添加/展示控件（用于编辑）
 * Author：Zero
 * Date：2021/12/28 10:38
 *
 * @version v6.1.1
 */
public class PictureAdderView2 extends RecyclerView implements PictureAdderAdapter2.OnAddPictureClickListener, OnItemClickListener2 {

    private PictureAdderAdapter2 adapter;

    public BindingCommand<List<LocalMedia>> onResultCommand;
    public BindingCommand<Integer> onItemClickListener;

    private ObservableList<Picture> selectPictures = new ObservableArrayList<>();

    private int numColumns;
    private int spacing;
    private int chooseMode;
    private int maxSelectNum;

    public PictureAdderView2(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        parseStyle(context, attrs);
        init(context);
    }

    private void parseStyle(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PictureAdderView2);
        spacing = (int) typedArray.getDimension(R.styleable.PictureAdderView2_spacing, SizeUtils.dp2px(10));
        chooseMode = typedArray.getInt(R.styleable.PictureAdderView2_chooseMode, 1);
        numColumns = typedArray.getInteger(R.styleable.PictureAdderView2_numColumns, 4);
        maxSelectNum = typedArray.getInteger(R.styleable.PictureAdderView2_maxSelectNum, 9);
    }

    private void init(@NonNull Context context) {
        setOverScrollMode(OVER_SCROLL_NEVER);
        FullyGridLayoutManager2 manager = new FullyGridLayoutManager2(context, numColumns, GridLayoutManager.VERTICAL, false);
        setLayoutManager(manager);
        addItemDecoration(new GridSpacingItemDecoration(numColumns, spacing, false));
        adapter = new PictureAdderAdapter2(context, this);
        adapter.setOnItemClickListener(this);
        adapter.setSelectMax(maxSelectNum);
        setAdapter(adapter);
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onAddPictureClick() {
        PictureSelector.create(ActivityUtils.getTopActivity())
                .openGallery(chooseMode)    // 扫描文件类型
                .setSelectorUIStyle(PsUI.createPsUI()) // 相册启动退出动画
                .setImageEngine(GlideEngine.createGlideEngine())   // 图片加载引擎
                .setCompressEngine(CompressEngine.createCompressEngine())   // 压缩加载引擎
                .setSandboxFileEngine(SandboxEngine.createSandboxEngine())
                .isWithSelectVideoImage(true) // 图片和视频是否可以同选,只在ofAll模式下有效
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)  // 相册Activity方向
                .isOriginalControl(false)  // 不显示原图控制按钮
                .setMaxSelectNum(maxSelectNum) // 最大选择数量
                .setSelectionMode(SelectModeConfig.MULTIPLE)    // 多选
                .setFilterVideoMaxSecond(30) // 显示多少秒以内的视频or音频
                .setRecordVideoMaxSecond(30)  //  视频录制秒数
                .forResult(new OnResultCallbackListener<>() {

                    @Override
                    public void onResult(ArrayList<LocalMedia> result) {
                        if (onResultCommand != null) onResultCommand.execute(result);
                    }

                    @Override
                    public void onCancel() {

                    }
                });   // 回调
    }

    @Override
    public void onItemClick(int position, View v) {
        if (onItemClickListener != null) onItemClickListener.execute(position);
    }

    public void setImage(List<Picture> result) {
        selectPictures.clear();
        selectPictures.addAll(result);
        adapter.setPictures(selectPictures);
    }

    public ObservableList<Picture> getSelectPictures() {
        return selectPictures;
    }

    /**
     * 设置上传图片回调
     *
     * @param onResultCommand
     */
    public void setOnResultCommand(BindingCommand<List<LocalMedia>> onResultCommand) {
        this.onResultCommand = onResultCommand;
    }

    /**
     * 设置删除图片回调
     *
     * @param onRemoveCommand
     */
    public void setOnRemovedCommand(BindingCommand<List<Picture>> onRemoveCommand) {
        adapter.setOnRemovedCommand(onRemoveCommand);
    }
}
