package com.toocms.tab.widget.pictureadder2;

/**
 * @Description: 上传照片返回的数据类型
 * @Author: Zero
 * @Version: 6.1.1
 */
public class Picture {

    public String id;
    public String abs_url;
    public String name;

    @Override
    public String toString() {
        return "Picture{" +
                "id='" + id + '\'' +
                ", abs_url='" + abs_url + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
