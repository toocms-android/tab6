package com.toocms.tab.imageload.engine;

import android.content.Context;

import com.luck.picture.lib.engine.UriToFileTransformEngine;
import com.luck.picture.lib.interfaces.OnKeyValueResultCallbackListener;
import com.luck.picture.lib.utils.SandboxTransformUtils;

/**
 * @Description:
 * @Author: Zero
 * @CreateDate: 2023/5/20 18:13
 */
public class SandboxFileEngine implements UriToFileTransformEngine {
    @Override
    public void onUriToFileAsyncTransform(Context context, String srcPath, String mineType, OnKeyValueResultCallbackListener call) {
        if (call != null) {
            String sandboxPath = SandboxTransformUtils.copyPathToSandbox(context, srcPath, mineType);
            call.onCallback(srcPath, sandboxPath);
        }
    }
}
