package com.toocms.tab.imageload.engine;

import android.content.Context;

import com.luck.picture.lib.engine.UriToFileTransformEngine;
import com.luck.picture.lib.interfaces.OnKeyValueResultCallbackListener;
import com.luck.picture.lib.utils.SandboxTransformUtils;

/**
 * @Description:
 * @Author: Zero
 * @CreateDate: 2023/6/8 14:20
 */
public class SandboxEngine implements UriToFileTransformEngine {

    @Override
    public void onUriToFileAsyncTransform(Context context, String srcPath, String mineType, OnKeyValueResultCallbackListener call) {
        if (call != null) {
            call.onCallback(srcPath, SandboxTransformUtils.copyPathToSandbox(context, srcPath, mineType));
        }
    }

    private SandboxEngine() {
    }

    private static final class InstanceHolder {
        static final SandboxEngine instance = new SandboxEngine();
    }

    public static SandboxEngine createSandboxEngine() {
        return SandboxEngine.InstanceHolder.instance;
    }
}
