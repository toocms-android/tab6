package com.toocms.tab.imageload.psui;

import com.luck.picture.lib.style.PictureSelectorStyle;
import com.luck.picture.lib.style.PictureWindowAnimationStyle;
import com.toocms.tab.R;

/**
 * @Description:
 * @Author: Zero
 * @CreateDate: 2023/5/20 17:14
 */
public class PsUI extends PictureSelectorStyle {

    private PsUI() {
        PictureWindowAnimationStyle pictureStyle = new PictureWindowAnimationStyle();
        pictureStyle.activityEnterAnimation = R.anim.slide_in_right;
        pictureStyle.activityExitAnimation = R.anim.slide_out_right;
        setWindowAnimationStyle(pictureStyle);
    }

    private static final class InstanceHolder {
        static final PsUI instance = new PsUI();
    }

    public static PsUI createPsUI() {
        return PsUI.InstanceHolder.instance;
    }
}
