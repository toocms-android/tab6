package com.toocms.tab.imageload.engine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.engine.CropFileEngine;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.utils.DateUtils;
import com.toocms.tab.configs.FileManager;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropImageEngine;

import java.io.File;
import java.util.ArrayList;

/**
 * @Description:
 * @Author: Zero
 * @CreateDate: 2023/5/20 18:10
 */
public class CropEngine implements CropFileEngine {

    private volatile static CropEngine instance;
    public int aspect_ratio_x, aspect_ratio_y;

    public static CropEngine createCropEngine(int aspect_ratio_x, int aspect_ratio_y) {
        if (instance == null)
            synchronized (CropEngine.class) {
                if (instance == null)
                    instance = new CropEngine(aspect_ratio_x, aspect_ratio_y);
            }
        return instance;
    }

    private CropEngine(int aspect_ratio_x, int aspect_ratio_y) {
        this.aspect_ratio_x = aspect_ratio_x;
        this.aspect_ratio_y = aspect_ratio_y;
    }

    @Override
    public void onStartCrop(Fragment fragment, Uri srcUri, Uri destinationUri, ArrayList<String> dataSource, int requestCode) {
        UCrop uCrop = UCrop.of(srcUri, destinationUri, dataSource);
        uCrop.withOptions(buildOptions());
        uCrop.setImageEngine(new CropImageEngine());
        uCrop.start(fragment.requireActivity(), fragment, requestCode);
    }

    private UCrop.Options buildOptions() {
        UCrop.Options options = new UCrop.Options();
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(true);
        options.setShowCropFrame(true);
        options.setShowCropGrid(true);
        options.setCircleDimmedLayer(false);
        options.withAspectRatio(aspect_ratio_x, aspect_ratio_y);
        options.isCropDragSmoothToCenter(false);
        options.isForbidCropGifWebp(true);
        options.isForbidSkipMultipleCrop(true);
        options.setMaxScaleMultiplier(100);
        return options;
    }

    private static class CropImageEngine implements UCropImageEngine {

        @Override
        public void loadImage(Context context, String url, ImageView imageView) {
            Glide.with(context).load(url).into(imageView);
        }

        @Override
        public void loadImage(Context context, Uri url, int maxWidth, int maxHeight, OnCallbackListener<Bitmap> call) {
            Glide.with(context).asBitmap().load(url).override(maxWidth, maxHeight).into(new CustomTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    if (call != null) call.onCall(resource);
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {
                    if (call != null) call.onCall(null);
                }
            });
        }
    }
}
