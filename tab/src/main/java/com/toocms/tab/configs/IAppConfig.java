package com.toocms.tab.configs;

import android.app.Application;

import com.toocms.tab.network.PostFormApi;
import com.toocms.tab.network.encrypt.ApiEncryptOption;

import okhttp3.OkHttpClient;
import rxhttp.wrapper.param.Param;

/**
 * App配置接口
 * <p>
 * Author Zero
 * Data 2020/11/06
 */
public interface IAppConfig {

    /**
     * 获取Http主URL
     *
     * @return
     */
    String getBaseUrl();

    /**
     * 获取更新的URL
     *
     * @return
     */
    String getUpdateUrl();

    /**
     * 获取友盟的AppKey
     *
     * @return
     */
    String getUmengAppkey();

    /**
     * 获取友盟推送服务的Secret
     *
     * @return
     */
    String getUmengPushSecret();

    /**
     * 设置RxHttp请求时的公共参数
     * 可根据不同请求添加不同参数，每次发送请求前都会被回调
     * 如果希望部分请求不回调这里，添加参数时调用setAssemblyEnabled(false)即可
     *
     * @return
     */
    Param<?> setOnParamAssembly(Param<?> param);

    /**
     * 设置Http请求参数加密（仅限POST请求方式）
     *
     * @return null为不加密
     */
    ApiEncryptOption getApiEncryptOption();

    /**
     * 自定义okHttp参数
     *
     * @param builder
     */
    void customOkHttp(OkHttpClient.Builder builder);

    /**
     * 是否自定义版本更新，若返回true则需在{@link #initJarForWeApplication(Application)}回调中自定义
     *
     * @return
     */
    boolean isCustomUpdate();

    /**
     * 在WeApplication中做第三方Jar包的初始化操作
     */
    void initJarForWeApplication(Application application);
}
