package com.toocms.tab.binding.viewadapter.webview;

import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.databinding.BindingAdapter;

public class ViewAdapter {

    @BindingAdapter({"render"})
    public static void loadHtml(WebView webView, final String html) {
        if (!TextUtils.isEmpty(html)) {
            webView.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
        }
    }

    @BindingAdapter({"loadUrl"})
    public static void loadUrl(WebView webView, final String url) {
        if (!TextUtils.isEmpty(url)) {
            webView.loadUrl(url);
            WebSettings webSettings = webView.getSettings();
            //是否开启JS支持
            webSettings.setJavaScriptEnabled(true);
            //是否允许JS打开新窗口
            webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
            webSettings.setDomStorageEnabled(true); //开启本地缓存，适应免责声明中的web
        }
    }
}
