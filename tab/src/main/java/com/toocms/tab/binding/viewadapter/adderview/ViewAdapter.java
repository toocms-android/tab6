package com.toocms.tab.binding.viewadapter.adderview;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableList;

import com.luck.picture.lib.entity.LocalMedia;
import com.toocms.tab.binding.command.BindingCommand;
import com.toocms.tab.widget.pictureadder.PictureAdderView;
import com.toocms.tab.widget.pictureadder2.Picture;
import com.toocms.tab.widget.pictureadder2.PictureAdderView2;

import java.util.List;

/**
 * Author：Zero
 * Date：2021/4/16
 */
public class ViewAdapter {

    @Deprecated
    @BindingAdapter(value = {"onDataChange"}, requireAll = false)
    public static void onDataChange(PictureAdderView pictureAdderView, BindingCommand<ObservableList<LocalMedia>> command) {
        pictureAdderView.getSelectList().addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<LocalMedia>>() {
            @Override
            public void onChanged(ObservableList<LocalMedia> sender) {
            }

            @Override
            public void onItemRangeChanged(ObservableList<LocalMedia> sender, int positionStart, int itemCount) {
            }

            @Override
            public void onItemRangeInserted(ObservableList<LocalMedia> sender, int positionStart, int itemCount) {
                command.execute(sender);
            }

            @Override
            public void onItemRangeMoved(ObservableList<LocalMedia> sender, int fromPosition, int toPosition, int itemCount) {
            }

            @Override
            public void onItemRangeRemoved(ObservableList<LocalMedia> sender, int positionStart, int itemCount) {
                command.execute(sender);
            }
        });
    }

    @BindingAdapter(value = {"onResultCommand"}, requireAll = false)
    public static void setPictureAdderCommand(PictureAdderView pictureAdderView, BindingCommand<List<LocalMedia>> onResultCommand) {
        if (onResultCommand != null) pictureAdderView.setOnResultCommand(onResultCommand);
    }

    @BindingAdapter(value = {"items", "onResultCommand", "onRemoveCommand"}, requireAll = false)
    public static void setPictureAdderCommand(PictureAdderView2 pictureAdderView2, List<Picture> pictureItems,
                                              BindingCommand<List<LocalMedia>> onResultCommand,
                                              BindingCommand<List<Picture>> onRemoveCommand) {
        pictureAdderView2.setImage(pictureItems);
        if (onResultCommand != null) pictureAdderView2.setOnResultCommand(onResultCommand);
        if (onRemoveCommand != null) pictureAdderView2.setOnRemovedCommand(onRemoveCommand);
    }
}
